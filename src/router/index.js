import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/pageLogin/loginComponent'
import register from '@/components/pageLogin/registerComponent'
import dashboard from '@/components/dashboardComponent'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'loginComponent',
      component: login
    },
    {
      path: '/',
      name: 'dashboard',
      component: dashboard
    },
    {
      path:'/register',
      name:'register',
      component:register
    }
  ]
})
